window.editLi = function(e) {

    // This is added as an eventHandler for al li's. (if click on text, then editLi) in main.js

    e.preventDefault(); // prevent deafult in old browsers.


    // get currentLi.innerHTML
    var currentLi = this;
    var currentContent = this.innerHTML;

    // Create input with currentLi.innerHTML as contents and update button
    var updateButton = document.createElement("input");
    updateButton.type = "button";
    updateButton.value = "update";
    updateButton.id = "updateBtn";
    // updateButton.setAttribute("onkeypress", "uniCharCode(event)")


    // Create deleteButton
    var deleteButton = document.createElement("input");
    deleteButton.type = "button";
    deleteButton.value = "delete";
    deleteButton.id = "deleteBtn";

    //create input field
    var currentInput = document.createElement("input");
    currentInput.classList.add("input");
    currentInput.value = currentContent;

    // hide current li with hide class to show only input
    this.classList.add("hide");

    // insert button and input before hidden li to show them.
    parentOfItemslist.insertBefore(currentInput, this);
    parentOfItemslist.insertBefore(deleteButton, currentInput.nextSibling);
    parentOfItemslist.insertBefore(updateButton, currentInput.nextSibling);

    // on enter update li
    function onEnterUpdate(event) {
        if (event.which == 13 || event.keyCode == 13 ) {
            updateLi(currentLi, currentInput.value, currentInput, updateButton, deleteButton);
        }
        else if ((event.which == 9 || event.keyCode == 9) && !event.shiftKey ) {
            updateLi(currentLi, currentInput.value, currentInput, updateButton, deleteButton);
            if (currentLi.nextSibling != parentOfItemslist.lastChild) {
                simulate(currentLi.nextSibling, "click");
            }else{
                simulate(currentLi, "click");
            }
        } else if ((event.which == 9 || event.keyCode == 9) && event.shiftKey ) {
            updateLi(currentLi, currentInput.value, currentInput, updateButton, deleteButton);
              previousLiId = parseInt(currentLi.getAttribute("order-id"))-1;
              previousLi = listItems[previousLiId];
              console.log(previousLiId);
            if (previousLiId >= 0) {

                simulate(previousLi, "click");
            } else{
                simulate(currentLi, "click");
            }

        }
    }
    // if key pressed run onEnterUpdate to check which key and if enter updateLi
    document.onkeypress = onEnterUpdate;





    // When update button clicked execture updateLi
    document.getElementById("updateBtn").onclick = function() {
        updateLi(currentLi, currentInput.value, currentInput, updateButton, deleteButton);
    };

    // When delete button clicked execture updateLi
    document.getElementById("deleteBtn").onclick = function() {
        removeLi(currentLi, currentInput, updateButton, deleteButton);
    };

    // focus on created input field.
    currentInput.focus();
    currentInput.setSelectionRange(0, currentInput.value.length);


}

window.updateLi = function(currentLi, newValue, currentInput, updateButton, deleteButton) {

    // lget content of current Li
    liHTML = currentLi.innerHTML;

    // set new value form input to current li
    currentLi.innerHTML = newValue;

    // remove hide class from current li to show it again and remove input and updatebutton
    currentLi.classList.remove("hide");
    currentInput.remove();
    updateButton.remove();
    deleteButton.remove();

    // update cookie with createCookie
    createCookie(document);

}



window.addLi = function() {

        // create new li element
        var newLi = document.createElement("li");
        newLi.classList.add("listItem");
        newLi.draggable = "true";

        // set li content empty
        newLi.innerHTML = "";

        // add li in listItems list as firstChild
        parentOfItemslist.insertBefore(newLi, parentOfItemslist.firstChild);

        // rescan for listItems
        listItems = document.querySelectorAll('.listItem');

        // evoke addEventListeners function on new list
        addEventListeners(listItems);

        // simulate click to be able to fill in value for new li (uses simulateEvents.js
        // src: http://stackoverflow.com/questions/6157929/how-to-simulate-a-mouse-click-using-javascript)
        simulate(listItems[0], "click");


        // createCookie(document);


    }
    // document.getElementById("clickMe").onclick = addLi(parentOfItemslist);





window.removeLi = function(currentLi, currentInput, updateButton, deleteButton) {

    // remove li input and buttons
    currentLi.remove();
    currentInput.remove();
    updateButton.remove();
    deleteButton.remove();

    // update cookie with createCookie
    createCookie(document);
}

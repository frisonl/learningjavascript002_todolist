// code by Friso NL - frisog at gmail .com

// events to create according ot: http://www.html5rocks.com/en/tutorials/dnd/basics/
//
// dragstart
// drag
// dragenter
// dragleave
// dragover
// drop
// dragend


window.onload = function() {





    window.parentOfItemslist = document.getElementById('checklist');
    // console.log(listItems);


    window.addEventListeners = function(listItems) {
        for (i = 0; i < listItems.length; i++) {
            window.listItem = listItems[i];


            listItem.setAttribute("order-id", i);



            listItem.addEventListener('dragstart', handleDragStart, false)
            listItem.addEventListener('dragenter', handleDragEnter, false)
            listItem.addEventListener('dragover', handleDragOver, false)
            listItem.addEventListener('dragleave', handleDragLeave, false)
            listItem.addEventListener('drop', handleDrop, false)
            listItem.addEventListener('dragend', handleDragEnd, false)
            listItem.addEventListener('click', editLi, false)
        }
    };




    window.listItems; // window.variable makes it global! instead of var = ...;

    window.createListInitial = function(e) {

        var listItmesWithoutExtra = document.querySelectorAll('.listItem');
        console.log(listItmesWithoutExtra);
        var extraLi = document.createElement("LI");

        extraLi.classList.add("ghostLi", "listItem");
        console.log(extraLi);
        var arrayListItmesWithoutExtra = Array.prototype.slice.call(listItmesWithoutExtra, 0);

        arrayListItmesWithoutExtra.push(extraLi);
        listItems = arrayListItmesWithoutExtra;

        console.log(listItems);
        window.parentOfItemslist = document.getElementById('checklist');
        console.log(parentOfItemslist);
        parentOfItemslist.innerHTML = "";
        for (i = 0; i < listItems.length; i++) {
            parentOfItemslist.appendChild(listItems[i]);
        }
        console.log(listItems);
        addEventListeners(listItems);
    };





    window.checkCurrentListItems = function(e) {
        var listItems = document.querySelectorAll('.listItem');
        return listItems;
    }

    var dragSrcEl = null;

    function handleDragStart(e) {
        this.className += " dragStartClass";
        dragSrcEl = this;

        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.innerHTML);
        // e.dataTransfer.setDragClass("dataTransferClass");

    }

    function handleDragOver(e) {
        // if (e.preventDefault) { not needed according to my question and anwers on : http://stackoverflow.com/questions/36920665/why-if-statement-with-e-preventdefault-drag-and-drop-javascript
        e.preventDefault();
        // }
        e.dataTransfer.dropEffect = 'move'; // sets cursor
        return false;

    }

    function handleDragEnter(e) {

        if (this != parentOfItemslist.lastChild && dragSrcEl.nextSibling != this && dragSrcEl != this) {
            // this / e.target is the current hover target.
            this.classList.add('over');
            parentOfItemslist.lastChild.classList.add("hideListChild");
        };
    }

    function handleDragLeave(e) {
        this.classList.remove('over'); // this / e.target is previous target element.
        parentOfItemslist.lastChild.classList.remove("hideListChild");

    }

    window.handleDrop = function(e) {


        checkCurrentListItems();
        e.stopPropagation(); // stops the browser from redirecting.
        dragSrcOrderId = parseInt(dragSrcEl.getAttribute("order-id"));
        dragTargetOrderId = parseInt(this.getAttribute("order-id"));
        var tempThis = this;


        // Don't do anything if dropping the same column we're dragging.
        // and
        // check if only one difference and then do not execute
        // && ((Math.abs(dragSrcOrderId - dragTargetOrderId)) != 1)
        if (dragSrcEl != this) {
            // Set the source column's HTML to the HTML of the column we dropped on.
            var tempThis = this;

            function makeNewOrderIds(tempThis) {
                // check if up or down movement

                dragSrcEl.setAttribute("order-id", dragTargetOrderId);
                tempThis.setAttribute("order-id", dragTargetOrderId);

                //  find divs between old and new location and set new ids - different in up or down movement (if else)
                if (dragSrcOrderId < dragTargetOrderId) {
                    for (i = dragSrcOrderId + 1; i < dragTargetOrderId; i++) {
                        listItems[i].setAttribute("order-id", i - 1);
                        // set new id src
                        dragSrcEl.setAttribute("order-id", dragTargetOrderId - 1);
                    }
                } else {
                    for (i = dragTargetOrderId; i < dragSrcOrderId; i++) {
                        listItems[i].setAttribute("order-id", i + 1);
                        // set new id src
                        dragSrcEl.setAttribute("order-id", dragTargetOrderId);

                    }
                }

            };
            makeNewOrderIds(tempThis);


            dragSrcEl.classList.remove("dragStartClass");
            this.classList.remove('over'); // this / e.target is previous target element.
            parentOfItemslist.lastChild.classList.remove("hideListChild");
            reOrder(listItems);




        } else {

            dragSrcEl.classList.remove("dragStartClass");
            return false;

        }

    };

    function handleDragEnd(e) {

        for (i = 0; i < listItems.length; i++) {
            listItem = listItems[i];
            listItem.classList.remove('over');
        }
        dragSrcEl.classList.remove("dragStartClass");


    }



    window.reOrder = function(listItems) {


        var tempListItems = listItems;
        tempListItems = Array.prototype.slice.call(tempListItems, 0);

        tempListItems.sort(function(a, b) {
            return a.getAttribute("order-id") - b.getAttribute("order-id");
        });



        window.parentOfItemslist = document.getElementById('checklist');
        parentOfItemslist.innerHTML = "";

        for (var i = 0, l = tempListItems.length; i < l; i++) {
            parentOfItemslist.appendChild(tempListItems[i]);
        }

        createCookie(document);

    };

    CheckIfCookie(document);



};
